package com.auditlog.boot.util;

import com.auditlog.boot.config.AuditLogProperties;

public interface Constants {

    String EXCLUDES = AuditLogProperties.PREFIX + ".excludes";
    String SUPPORT_INSERT_UPDATE = AuditLogProperties.PREFIX + "。supportInsertUpdate";
    String ONLY_DIFF = AuditLogProperties.PREFIX + ".onlyDiff";
}
