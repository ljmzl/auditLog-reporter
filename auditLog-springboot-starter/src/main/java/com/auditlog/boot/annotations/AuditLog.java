package com.auditlog.boot.annotations;

import java.lang.annotation.*;

/**
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/3 18:17
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuditLog {

    /**
     * 在执行日志记录异常时，是否继续执行目标sql
     */
    boolean onExceptionContinue() default false;

    String operationName() default "";

    /**
     * 不记录日志的表
     */
    String[] ignoreTables() default {};

    boolean caseSensitive() default false;
}
