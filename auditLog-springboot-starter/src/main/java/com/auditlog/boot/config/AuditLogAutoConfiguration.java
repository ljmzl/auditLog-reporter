package com.auditlog.boot.config;

import com.auditlog.boot.aop.AuditLogAspect;
import com.auditlog.boot.aop.datasource.AuditLogDataSourceProxyCreator;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/3 18:15
 * @version 1.0
 */
@Configuration
@EnableConfigurationProperties(AuditLogProperties.class)
public class AuditLogAutoConfiguration {

    @Bean
    public AuditLogAspect auditLogAspect(AuditLogProperties auditLogProperties) {
        return new AuditLogAspect(auditLogProperties);
    }

    @Bean
    public AuditLogDataSourceProxyCreator auditLogDataSourceProxyCreator(AuditLogProperties properties) {
        return new AuditLogDataSourceProxyCreator(properties);
    }

    @Bean
    public RegistryConfiguration operationUserHandler() {
        return new RegistryConfiguration();
    }

}
