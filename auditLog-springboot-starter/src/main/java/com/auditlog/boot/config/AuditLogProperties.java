package com.auditlog.boot.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashSet;
import java.util.Set;

@ConfigurationProperties(prefix = AuditLogProperties.PREFIX)
@Data
@NoArgsConstructor
public class AuditLogProperties implements InitializingBean {
    public static final String PREFIX = "audit.log";

    /**
     * 设置不启用代理的datasource类名
     */
    private String[] excludes = new String[]{};

    /**
     * 是否支持mysql中on dumplication key update更新
     */
    private boolean supportInsertUpdate;

    /**
     * 是否只记录被更新的字段（true：是，false：记录表中所有字段）
     */
    private boolean onlyDiff;

    /**
     * 异常时是否继续执行原sql
     */
    private boolean onExceptionContinue = false;

    /**
     * 全局忽略表，如果有schema，请带上schema
     */
    private Set<String> ignoreTables;

    /**
     * 表名是否大小写敏感
     */
    private boolean caseSensitive;

    private Set<String> useCglibProxyClassName;

    @Override
    public void afterPropertiesSet() throws Exception {
    }
}
