package com.auditlog.boot.config;

import cn.hutool.core.collection.CollectionUtil;
import com.auditlog.converter.Converter;
import com.auditlog.converter.ConverterRegistry;
import com.auditlog.format.RecordImageFormat;
import com.auditlog.format.RecordImageFormatRegistry;
import com.auditlog.handler.OperationHandlerRegistry;
import com.auditlog.handler.storage.StorageHandler;
import com.auditlog.handler.meta.UserOperationHandler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/3 17:50
 * @version 1.0
 */
public class RegistryConfiguration implements InitializingBean {

    @Autowired(required = false)
    UserOperationHandler userOperationHandler;

    @Autowired(required = false)
    List<RecordImageFormat> recordImageFormats;

    @Autowired(required = false)
    List<Converter> converters;

    @Autowired(required = false)
    StorageHandler storageHandler;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (userOperationHandler != null) {
            OperationHandlerRegistry.getInstance().registerUserOperationHandler(userOperationHandler);
        }
        if (CollectionUtil.isNotEmpty(converters)) {
            converters.forEach(converter -> ConverterRegistry.getInstance().register(converter));
        }

        if (CollectionUtil.isNotEmpty(recordImageFormats)) {
            recordImageFormats.forEach(recordImageFormat -> RecordImageFormatRegistry.getInstance().register(recordImageFormat));
        }
        if (storageHandler != null) {
            OperationHandlerRegistry.getInstance().registerStorageHandler(storageHandler);
        }
    }
}
