package com.auditlog.boot.aop.datasource;

import com.auditlog.datasource.DataSourceProxy;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 11:29
 * @version 1.0
 */
public class DataSourceProxyHolder {

    private static final Map<DataSource, DataSourceProxy> PROXY_MAP = new HashMap<>(4);

    static DataSourceProxy put(DataSource origin, DataSourceProxy proxy) {
        return PROXY_MAP.put(origin, proxy);
    }

    static DataSourceProxy get(DataSource origin) {
        return PROXY_MAP.get(origin);
    }
}
