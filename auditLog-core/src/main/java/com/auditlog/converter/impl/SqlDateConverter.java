package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SqlDateConverter implements Converter<java.sql.Date> {
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE;

    @Override
    public boolean support(Class clazz) {
        return Date.class == clazz;
    }

    @Override
    public String toStr(Date date) throws SQLException {
        LocalDate localDate = date.toLocalDate();
        return dateTimeFormatter.format(localDate);
    }
}
