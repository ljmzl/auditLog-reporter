package com.auditlog.converter.impl;

import cn.hutool.core.codec.Base64;
import com.auditlog.converter.Converter;

import java.sql.Blob;
import java.sql.SQLException;

public class BlobConverter implements Converter<Blob> {
    @Override
    public boolean support(Class clazz) {
        return clazz == Blob.class;
    }

    @Override
    public String toStr(Blob blob) throws SQLException {
        byte[] bytes = blob.getBytes(1, (int) blob.length());
        return Base64.encode(bytes);
    }
}
