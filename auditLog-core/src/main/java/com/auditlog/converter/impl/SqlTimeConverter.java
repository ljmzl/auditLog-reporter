package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;

import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SqlTimeConverter implements Converter<java.sql.Time> {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_TIME;

    @Override
    public boolean support(Class clazz) {
        return Time.class == clazz;
    }

    @Override
    public String toStr(Time time) throws SQLException {
        LocalTime localTime = time.toLocalTime();
        return dateTimeFormatter.format(localTime);
    }
}
