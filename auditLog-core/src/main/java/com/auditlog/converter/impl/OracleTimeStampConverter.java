package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;
import oracle.sql.TIMESTAMP;

public class OracleTimeStampConverter implements Converter<TIMESTAMP> {

    @Override
    public boolean support(Class clazz) {
        return clazz == TIMESTAMP.class;
    }

    @Override
    public String toStr(TIMESTAMP timestamp) {
        return timestamp.stringValue();
    }
}
