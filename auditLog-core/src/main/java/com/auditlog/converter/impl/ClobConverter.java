package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;

import java.sql.Clob;
import java.sql.SQLException;

public class ClobConverter implements Converter<Clob> {
    @Override
    public boolean support(Class clazz) {
        return clazz == Clob.class;
    }

    @Override
    public String toStr(Clob clob) throws SQLException {
        return clob.getSubString(1, (int) clob.length());
    }
}
