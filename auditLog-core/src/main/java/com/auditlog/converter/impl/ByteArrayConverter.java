package com.auditlog.converter.impl;

import cn.hutool.core.codec.Base64;
import com.auditlog.converter.Converter;

import java.sql.SQLException;

public class ByteArrayConverter implements Converter<byte[]> {
    @Override
    public boolean support(Class clazz) {
        return byte[].class == clazz;
    }

    @Override
    public String toStr(byte[] bytes) throws SQLException {
        return Base64.encode(bytes);
    }
}
