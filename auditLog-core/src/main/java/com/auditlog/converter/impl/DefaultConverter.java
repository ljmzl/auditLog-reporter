package com.auditlog.converter.impl;

import cn.hutool.core.convert.impl.StringConverter;
import com.auditlog.converter.Converter;

/**
 * 默认的类型转换器
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/28 18:36
 */
public class DefaultConverter implements Converter {

    private StringConverter stringConverter = new StringConverter();

    @Override
    public boolean support(Class clazz) {
        return false;
    }

    @Override
    public String toStr(Object o) {
        return stringConverter.convert(o, "");
    }
}
