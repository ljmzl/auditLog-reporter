package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeConverter implements Converter<LocalDateTime> {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public boolean support(Class clazz) {
        return LocalDateTime.class == clazz;
    }

    @Override
    public String toStr(LocalDateTime localDateTime) throws SQLException {
        return dateTimeFormatter.format(localDateTime);
    }
}
