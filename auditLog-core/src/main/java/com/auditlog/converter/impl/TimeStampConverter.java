package com.auditlog.converter.impl;

import com.auditlog.converter.Converter;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeStampConverter implements Converter<Timestamp> {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");

    @Override
    public boolean support(Class clazz) {
        return Timestamp.class == clazz;
    }

    @Override
    public String toStr(Timestamp timestamp) throws SQLException {
        LocalDateTime dateTime = timestamp.toLocalDateTime();
        return dateTimeFormatter.format(dateTime);
    }
}
