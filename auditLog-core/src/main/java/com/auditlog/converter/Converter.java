package com.auditlog.converter;

import java.sql.SQLException;

/**
 * 类型转换器
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/28 18:34
 */
public interface Converter<T> {

    /**
     * 支持的类型
     *
     * @param clazz
     * @return: boolean
     */
    boolean support(Class clazz);

    default String convert2Str(T t) throws SQLException {
        if (t == null) {
            return null;
        }
        return toStr(t);
    }

    /**
     * 对象转换成字符串
     *
     * @param t
     * @return: java.lang.String
     */
    String toStr(T t) throws SQLException;

    /**
     * 字符串转换成对象
     *
     * @param str
     * @return: T
     */
    default T convert2Bean(String str) {
        return null;
    }

}
