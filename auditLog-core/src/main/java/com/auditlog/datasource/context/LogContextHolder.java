package com.auditlog.datasource.context;

/**
 * 配置上下文
 * @author Zhiyang.Zhang
 * @date 2023/9/23 18:00
 * @version 1.0
 */
public class LogContextHolder {

    private static final ThreadLocal<LogContext> CONTEXT_HOLDER = ThreadLocal.withInitial(() -> new LogContext());

    public static LogContext get() {
        return CONTEXT_HOLDER.get();
    }

    public static void clear() {
        CONTEXT_HOLDER.remove();
    }

    public static boolean isAudit() {
        return get().isAudit();
    }
}


