package com.auditlog.datasource.context;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.auditlog.datasource.db.SqlType;
import com.auditlog.sql.statistics.SqlStatistician;
import com.auditlog.datasource.struct.RecordImage;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * sql执行上下文
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/28 18:22
 */
@Data
public class ExecutionContext extends CommonField implements Cloneable {
    private String userId;
    private String userName;
    private List<RecordImage> executedRecordImages = new ArrayList<>();
    private SqlStatistician sqlStatistician = new SqlStatistician();
    private String operationId;
    private Map<String, Object> metaData;
    private String operationName;
    private boolean isAudit;
    private String dbType;
    private String resourceId;

    public void addRecordImage(RecordImage recordImage) {
        if (SqlType.ALL == recordImage.getSqlType()) {
            return;
        }
        this.executedRecordImages.add(recordImage);
    }

    @Override
    public void reset() {
        super.reset();
        this.userId = null;
        this.userName = null;
        this.dbType = null;
        this.resourceId = null;
        // this.isAudit = false;
        this.executedRecordImages = new ArrayList<>();
        this.sqlStatistician = new SqlStatistician();
        this.operationId = IdUtil.getSnowflakeNextIdStr();
    }

    public ExecutionContext copy() {
        ExecutionContext executionContext = new ExecutionContext();
        BeanUtil.copyProperties(this, executionContext);
        return executionContext;
    }
}
