package com.auditlog.datasource.context;

import lombok.Data;

import java.util.Set;

/**
 * 存放一些配置信息
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/3 18:14
 */
@Data
public class LogContext {
    private boolean isAudit;
    private boolean onExceptionContinue;
    private Set<String> ignoreTables;
    private boolean caseSensitive;
}
