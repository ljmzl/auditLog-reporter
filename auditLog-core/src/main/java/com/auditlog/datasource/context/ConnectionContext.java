package com.auditlog.datasource.context;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/29 11:09
 */
@Data
@NoArgsConstructor
@Slf4j
public class ConnectionContext extends CommonField implements Cloneable {
    /**
     * 是否是批处理
     */
    private boolean isBatch;
    /**
     * 预处理时是否一个sql中包含多个sql语句（如："sql1;sql2;sql3"）
     */
    private boolean isPrepareMultiSql;
    /**
     * 非预处理时是否一个sql中包含多个sql语句（如："sql1;sql2;sql3"）
     */
    private boolean isNonPrepareMultiSql;
    /**
     * 是否自动在预编译时自动加上RETURN_GENERATED_KEYS
     */
    private boolean autoAssignGeneratedKeys = true;
    /**
     * 发生异常时，是否继续执行
     */
    private boolean onExceptionContinue;

    private boolean changeAutoCommit;

    private String dbType;

    private String resourceId;

    public boolean isMultiOrBatch() {
        return this.isPrepareMultiSql || this.isNonPrepareMultiSql || this.isBatch;
    }

    @Override
    public void reset() {
        this.isBatch = false;
        this.isPrepareMultiSql = false;
        this.isNonPrepareMultiSql = false;
        this.autoAssignGeneratedKeys = true;
        this.changeAutoCommit = false;
        this.dbType = null;
        this.resourceId = null;
    }

    public void resetAll() {
        this.reset();
        super.reset();
    }
}
