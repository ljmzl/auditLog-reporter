package com.auditlog.datasource;

import javax.sql.DataSource;

public interface AuditLogDataSourceProxy extends DataSource {
    DataSource getTarget();
}
