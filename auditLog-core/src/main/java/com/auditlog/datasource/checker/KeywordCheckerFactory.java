package com.auditlog.datasource.checker;

import com.auditlog.util.JdbcConstants;

import java.util.HashMap;
import java.util.Map;

public class KeywordCheckerFactory {
    private static final Map<String,KeywordChecker> CHECKER_MAP = new HashMap<>();
    static {
        CHECKER_MAP.put(JdbcConstants.MYSQL_STR,new MySQLKeywordChecker());
        CHECKER_MAP.put(JdbcConstants.ORACLE_STR,new OracleKeywordChecker());
        CHECKER_MAP.put(JdbcConstants.POSTGRESQL_STR,new PostgresqlKeywordChecker());
    }

    public static KeywordChecker getChecker(String dbType){
        return CHECKER_MAP.get(dbType);
    }
}
