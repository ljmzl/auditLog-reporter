package com.auditlog.datasource.db;

public enum SqlType {
    EXCEPTION,
    MULTI,
    UPDATE,
    INSERT,
    INSERT_ALL,
    INSERT_FIRST,
    INSERT_CONDITION,
    DELETE,
    REPLACE,
    MERGE,
    INSERT_DUPLICATE,
    SELECT,
    ALL;
}
