package com.auditlog.datasource.table.extrator;

import com.auditlog.datasource.struct.TableInfo;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;

public class DeleteTableExtractor implements TableExtractor {

    @Override
    public Class support() {
        return Delete.class;
    }

    @Override
    public TableInfo extract(Statement statement) {
        Delete delete = (Delete) statement;
        net.sf.jsqlparser.schema.Table table = delete.getTable();
        return TableInfo.builder().name(getTableName(table)).alias(table.getAlias() == null ? "" : table.getAlias().getName()).build();
    }
}
