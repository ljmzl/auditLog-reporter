package com.auditlog.datasource.table.extrator;

import com.auditlog.datasource.struct.TableInfo;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

public class SelectTableExtractor implements TableExtractor {

    @Override
    public Class support() {
        return Select.class;
    }

    @Override
    public TableInfo extract(Statement statement) {
        return TableInfo.builder().build();
    }
}
