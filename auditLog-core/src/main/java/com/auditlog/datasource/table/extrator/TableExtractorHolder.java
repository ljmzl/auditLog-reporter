package com.auditlog.datasource.table.extrator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TableExtractorHolder {

    public static final Map<Class, TableExtractor> EXTRACTOR = new ConcurrentHashMap<>(8);

    static {
        register(new InsertTableExtractor());
        register(new UpdateTableExtractor());
        register(new SelectTableExtractor());
        register(new DeleteTableExtractor());
    }

    public static void register(TableExtractor extractor) {
        EXTRACTOR.putIfAbsent(extractor.support(), extractor);
    }

    public static TableExtractor extractor(Class type) {
        return EXTRACTOR.get(type);
    }
}
