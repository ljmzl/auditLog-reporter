package com.auditlog.datasource.table.cache;

import com.auditlog.util.JdbcConstants;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 11:27
 * @version 1.0
 */
public class TableMetaCacheFactory {

    private static final Map<String, TableMetaCache> TABLE_META_CACHE_MAP = new ConcurrentHashMap<>();

    static {
        TABLE_META_CACHE_MAP.put(JdbcConstants.ORACLE_STR,new OracleTableMetaCache());
        TABLE_META_CACHE_MAP.put(JdbcConstants.MYSQL_STR,new MysqlTableMetaCache());
        TABLE_META_CACHE_MAP.put(JdbcConstants.POSTGRESQL_STR,new PostgresqlTableMetaCache());
    }

    public static TableMetaCache getTableMetaCache(String dbType) {
        return TABLE_META_CACHE_MAP.get(dbType);
    }
}
