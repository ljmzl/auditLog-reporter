package com.auditlog.datasource.table.extrator;

import cn.hutool.core.util.StrUtil;
import com.auditlog.datasource.struct.TableInfo;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;

public interface TableExtractor {

    Class support();

    TableInfo extract(Statement statement);

    default String getTableName(Table table){
        StringBuilder tableName = new StringBuilder();
        if(StrUtil.isNotEmpty(table.getSchemaName())){
            tableName.append(table.getSchemaName()).append(".");
        }
        tableName.append(table.getName());
        return tableName.toString();
    }

}
