package com.auditlog.datasource.table.extrator;

import com.auditlog.datasource.struct.TableInfo;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.update.Update;

public class UpdateTableExtractor implements TableExtractor {

    @Override
    public Class support() {
        return Update.class;
    }

    @Override
    public TableInfo extract(Statement statement) {
        Update update = (Update) statement;
        net.sf.jsqlparser.schema.Table table = update.getTable();
        return TableInfo.builder().name(getTableName(table)).alias(table.getAlias() == null ? "" : table.getAlias().getName()).build();
    }
}
