package com.auditlog.datasource.table.extrator;

import com.auditlog.datasource.struct.TableInfo;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.insert.Insert;

public class InsertTableExtractor implements TableExtractor {

    @Override
    public Class support() {
        return Insert.class;
    }

    @Override
    public TableInfo extract(Statement statement) {
        Insert insert = (Insert) statement;
        net.sf.jsqlparser.schema.Table table = insert.getTable();
        return TableInfo.builder().name(getTableName(table)).alias(table.getAlias() == null ? "" : table.getAlias().getName()).build();
    }
}
