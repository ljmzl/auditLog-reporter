package com.auditlog.datasource.struct;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.datasource.table.TableMeta;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
@Builder
public class RecordImage {

    /**
     * 操作类型
     */
    private SqlType sqlType;

    /**
     * 可能被更新的主键列值集合（insert on duplicate语句时这个不准确，需通过后面的值比较来确定的具体更新列）<br></br>
     * 因为当待插入的一条数据的多个唯一key对应表中多条数据时，这里其实只有第一个key才是有效的
     */
    @Builder.Default
    private List<List<Object>> possibleUpdateOrDeletePks = new ArrayList<>();

    /**
     * 被插入的主键列值集合
     */
    @Builder.Default
    private List<List<Object>> insertPks = new ArrayList<>();

    /**
     * 主键列名集合
     */
    @Builder.Default
    private List<String> pkColumnsName = new ArrayList<>();

    /**
     * 被更新的列名集合
     */
    @Builder.Default
    private List<String> updateColumnsName = new ArrayList<>();

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表别名
     */
    private String alias;

    /**
     * 表中所有列集合
     */
    private List<String> allColumnsWithAlias;


    private TableMeta tableMeta;

    /**
     * 执行前数据
     */
    @Builder.Default
    private TableRecord beforeRecord = TableRecord.builder().build();

    /**
     * 执行后数据
     */
    @Builder.Default
    private TableRecord afterRecord = TableRecord.builder().build();

}
