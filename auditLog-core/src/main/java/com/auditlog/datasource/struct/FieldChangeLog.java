package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FieldChangeLog {
    private Object oldValue;
    private Object newValue;
    /**
     * 字段名
     */
    private String name;
}
