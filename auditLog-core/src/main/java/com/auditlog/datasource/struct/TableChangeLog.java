package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 表变更日志
 * @author Zhiyang.Zhang
 * @date 2022/12/2 22:38
 * @version 1.0
 */
@Builder
@Data
public class TableChangeLog {
    private List<RowChangeLog> rowChangeLogs;
    private String tableName;
}
