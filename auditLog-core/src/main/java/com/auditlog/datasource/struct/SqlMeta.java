package com.auditlog.datasource.struct;

import com.auditlog.datasource.db.SqlType;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sf.jsqlparser.statement.Statement;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class SqlMeta {
    private SqlType sqlType;
    private List<Statement> statements;
    private List<SqlType> sqlTypes = new ArrayList<>();

    public void addSqlType(SqlType sqlType) {
        this.sqlType = sqlType;
        sqlTypes.add(sqlType);
        if (sqlTypes.size() > 1) {
            this.sqlType = SqlType.MULTI;
        }
    }

    public boolean isAllType() {
        return sqlTypes.stream().allMatch(type -> type == SqlType.ALL);
    }

}
