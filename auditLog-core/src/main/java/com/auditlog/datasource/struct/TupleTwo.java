package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TupleTwo<V1, V2> {
    private V1 v1;
    private V2 v2;

    public boolean isEmpty() {
        return v1 == null || v2 == null;
    }
}
