package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class TableRecord {
    /**
     * 主键对应的值集合，key:行数据主键值组成，value：数据库行记录
     */
    @Builder.Default
    private Map<String, List<Object>> rows = new HashMap<>();

    @Builder.Default
    private String delimiter = "";

    public void addRow(String key, List<Object> row) {
        this.rows.put(key, row);
    }

    public void addRows(Map<String, List<Object>> rowDatas) {
        this.rows.putAll(rowDatas);
    }

    public int getAffectedRows() {
        return rows.size();
    }
}
