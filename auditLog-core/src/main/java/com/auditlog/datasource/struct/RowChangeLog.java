package com.auditlog.datasource.struct;

import com.auditlog.datasource.db.SqlType;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;


/**
 * 行数据
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/2 22:35
 */
@Data
@Builder
public class RowChangeLog {
    /**
     * 行数据变更日志
     */
    private List<FieldChangeLog> fieldChangeLogs;
    /**
     * 主键map
     */
    private Map<String, String> keyMap;

    private SqlType sqlType;
}
