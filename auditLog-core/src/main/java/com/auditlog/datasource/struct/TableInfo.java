package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TableInfo {
    private String name;
    private String alias;
}
