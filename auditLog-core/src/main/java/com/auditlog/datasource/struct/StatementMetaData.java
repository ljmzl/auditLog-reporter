package com.auditlog.datasource.struct;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.sql.runner.SqlRunner;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class StatementMetaData {
    private SqlType sqlType;
    private String tableRecordsSql;
    private List<Integer> placeHolderIndex;
    private String tableName;
    private SqlRunner sqlRunner;
}
