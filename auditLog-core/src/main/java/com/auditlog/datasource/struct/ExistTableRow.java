package com.auditlog.datasource.struct;

import lombok.Builder;
import lombok.Data;

import java.util.*;

/**
 * 行数据
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/28 18:25
 */
@Builder
@Data
public class ExistTableRow {

    /**
     * 主键和唯一键值组成的key
     */
    @Builder.Default
    private Set<String> allKeys = new HashSet<>();

    /**
     * 主键对应的值
     */
    @Builder.Default
    private List<List<Object>> pkValues = new ArrayList<>();

    /**
     * 主键列名
     */
    private List<String> pkColumns;

    @Builder.Default
    private Map<String, List<Object>> rows = new HashMap<>();

    public void addKey(String key) {
        this.allKeys.add(key);
    }

    public boolean existKey(String key) {
        return this.allKeys.contains(key);
    }

}
