package com.auditlog.sql.statistics;

import lombok.Data;

/**
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/29 11:15
 */
@Data
public class Quota {
    /**
     * 执行次数
     */
    private int executeCount;
    /**
     * 执行总耗时
     */
    private long executeTimes;
    /**
     * 最长耗时
     */
    private long executeMaxTime = Long.MIN_VALUE;
    /**
     * 最少耗时
     */
    private long executeMinTime = Long.MAX_VALUE;

    public void increase(long time) {
        executeCount++;
        if (time > executeMaxTime) {
            executeMaxTime = time;
        }
        if (time < executeMinTime) {
            executeMinTime = time;
        }
        executeTimes += time;
    }

    /**
     * 平均执行时间
     *
     * @return: long
     */
    public long avgExecuteTime() {
        return executeTimes / executeCount;
    }
}
