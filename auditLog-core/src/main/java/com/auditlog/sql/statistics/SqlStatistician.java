package com.auditlog.sql.statistics;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

/**
 * sql统计
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/29 11:13
 */
@ToString
public class SqlStatistician {

    @Setter
    @Getter
    private Map<String, Quota> sqlQuta = new HashMap<>(4);

    public void collect(String sql, long time) {
        Quota quota = sqlQuta.get(sql);
        if (quota == null) {
            quota = new Quota();
            sqlQuta.put(sql, quota);
        }
        quota.increase(time);
    }
}
