package com.auditlog.sql.runner;

/**
 * 自定义校验接口
 * @author Zhiyang.Zhang
 * @date 2022/11/13 20:54
 * @version 1.0
 */
public interface SqlValidator {

    default void validate() {

    }
}
