package com.auditlog.sql.runner;

import com.auditlog.sql.parser.StatementMetaParser;
import com.auditlog.datasource.struct.RecordImage;
import net.sf.jsqlparser.statement.Statement;

/**
 * sql执行器
 * @author Zhiyang.Zhang
 * @date 2022/11/14 12:21
 * @version 1.0
 */
public interface SqlRunner<S extends Statement> extends SqlValidator, StatementMetaParser<S> {

    RecordImage beforeImage() throws Exception;

    RecordImage afterImage(RecordImage beforeRecordImage) throws Exception;

}
