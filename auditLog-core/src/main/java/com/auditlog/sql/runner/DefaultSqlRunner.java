package com.auditlog.sql.runner;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.datasource.db.SqlType;
import com.auditlog.datasource.struct.RecordImage;
import com.auditlog.datasource.struct.StatementMetaData;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.statement.Statement;

/**
 * <br>用来处理Audit Log不支持的Replce、Merge等操作，打印日志</br>
 * <br></br>
 * <br></br>
 * @author Zhiyang.Zhang
 * @date 2022/10/27 9:31
 * @version 1.0
 */
@Slf4j
public class DefaultSqlRunner<T extends java.sql.Statement> extends AbstractSqlRunner<T, Statement> {

    public DefaultSqlRunner(StatementProxy<T> statementProxy, Statement statement) {
        super(statementProxy, statement);
    }

    @Override
    public String getAlias() {
        return "";
    }

    @Override
    public String getTableName() {
        return "";
    }

    @Override
    public StatementMetaData getMetaData(Statement statement) {
        return StatementMetaData.builder().build();
    }

    @Override
    public RecordImage beforeImage() throws Exception{
        log.debug("忽略sql: {}", this.getStatement().toString());
        return RecordImage.builder().sqlType(SqlType.ALL).build();
    }

    @Override
    public RecordImage afterImage(RecordImage beforeRecordImage) throws Exception{
        return beforeRecordImage;
    }
}
