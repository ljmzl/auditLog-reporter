package com.auditlog.sql.context;

import com.auditlog.datasource.db.SqlType;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 12:41
 * @version 1.0
 */
@Data
@Builder
public class ResultSqlContext {
    private String sql;
    private List<Integer> placeHolderIndex;
    private SqlType sqlType;
    private String tableName;
    private String alias;
    private List<String> pkNames;
}
