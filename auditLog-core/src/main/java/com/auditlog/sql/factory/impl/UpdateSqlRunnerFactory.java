package com.auditlog.sql.factory.impl;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.sql.factory.SqlRunnerFactory;
import com.auditlog.sql.runner.SqlRunner;
import com.auditlog.datasource.db.SqlType;
import com.auditlog.sql.runner.UpdateSqlRunner;
import net.sf.jsqlparser.statement.update.Update;

import java.sql.Statement;

public class UpdateSqlRunnerFactory<T extends Statement> implements SqlRunnerFactory<T> {

    @Override
    public SqlRunner getRunner(StatementProxy<T> statementProxy, net.sf.jsqlparser.statement.Statement statement) {
        return new UpdateSqlRunner<T>(statementProxy,(Update) statement);
    }

    @Override
    public boolean support(net.sf.jsqlparser.statement.Statement statement) {
        return statement instanceof Update;
    }

    @Override
    public SqlType sqlType() {
        return SqlType.UPDATE;
    }
}
