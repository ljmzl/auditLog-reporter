package com.auditlog.sql.factory;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.sql.runner.SqlRunner;
import com.auditlog.datasource.db.SqlType;
import net.sf.jsqlparser.statement.Statement;

public interface SqlRunnerFactory<T extends java.sql.Statement> {

    /**
     获取一个SqlRunner实例
     * @param statementProxy
 * @param statement
     * @return: com.auditlog.sql.runner.SqlRunner
     */
    SqlRunner getRunner(StatementProxy<T> statementProxy, Statement statement);

    /**
     是否支持指定的statement
     * @param statement
     * @return: boolean
     */
    boolean support(Statement statement);

    /**
     支持的SqlType
     *
     * @return: com.auditlog.datasource.db.SqlType
     */
    SqlType sqlType();
}
