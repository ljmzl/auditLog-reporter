package com.auditlog.sql.factory.impl;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.sql.factory.SqlRunnerFactory;
import com.auditlog.sql.runner.InsertSqlRunner;
import com.auditlog.sql.runner.SqlRunner;
import com.auditlog.datasource.db.SqlType;
import net.sf.jsqlparser.statement.insert.Insert;

import java.sql.Statement;

public class InsertSqlRunnerFactory<T extends Statement> implements SqlRunnerFactory<T> {

    @Override
    public SqlRunner getRunner(StatementProxy<T> statementProxy, net.sf.jsqlparser.statement.Statement statement) {
        return new InsertSqlRunner<T>(statementProxy, (Insert) statement);
    }

    @Override
    public boolean support(net.sf.jsqlparser.statement.Statement statement) {
        if (statement instanceof Insert) {
            Insert insert = (Insert) statement;
            return !insert.isUseDuplicate();

        } else {
            return false;
        }
    }

    @Override
    public SqlType sqlType() {
        return SqlType.INSERT;
    }
}
