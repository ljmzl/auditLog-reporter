package com.auditlog.sql.factory.impl;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.sql.factory.SqlRunnerFactory;
import com.auditlog.sql.runner.DeleteSqlRunner;
import com.auditlog.sql.runner.SqlRunner;
import com.auditlog.datasource.db.SqlType;
import net.sf.jsqlparser.statement.delete.Delete;

import java.sql.Statement;

public class DeleteSqlRunnerFactory<T extends Statement> implements SqlRunnerFactory<T> {

    @Override
    public SqlRunner getRunner(StatementProxy<T> statementProxy, net.sf.jsqlparser.statement.Statement statement) {
        return new DeleteSqlRunner<T>(statementProxy,(Delete) statement);
    }

    @Override
    public boolean support(net.sf.jsqlparser.statement.Statement statement) {
        return statement instanceof Delete;
    }

    @Override
    public SqlType sqlType() {
        return SqlType.DELETE;
    }
}
