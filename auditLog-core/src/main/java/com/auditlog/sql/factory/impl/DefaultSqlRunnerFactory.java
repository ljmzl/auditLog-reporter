package com.auditlog.sql.factory.impl;

import com.auditlog.datasource.StatementProxy;
import com.auditlog.sql.factory.SqlRunnerFactory;
import com.auditlog.sql.runner.DefaultSqlRunner;
import com.auditlog.sql.runner.SqlRunner;
import com.auditlog.datasource.db.SqlType;

import java.sql.Statement;

public class DefaultSqlRunnerFactory<T extends Statement> implements SqlRunnerFactory<T> {

    @Override
    public SqlRunner getRunner(StatementProxy<T> statementProxy, net.sf.jsqlparser.statement.Statement statement) {
        return new DefaultSqlRunner<T>(statementProxy, statement);
    }

    @Override
    public boolean support(net.sf.jsqlparser.statement.Statement statement) {
        return false;
    }

    @Override
    public SqlType sqlType() {
        return SqlType.ALL;
    }
}
