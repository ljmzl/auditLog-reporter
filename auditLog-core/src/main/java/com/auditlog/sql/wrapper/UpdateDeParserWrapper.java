package com.auditlog.sql.wrapper;

import cn.hutool.core.collection.CollectionUtil;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.statement.update.UpdateSet;
import net.sf.jsqlparser.util.deparser.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UpdateDeParserWrapper extends UpdateDeParser implements DeParsrWrapper<Update> {

    private StringBuilder buffer = new StringBuilder();

    List<Integer> validPlaceholder = new ArrayList<>();

    List<Integer> invalidPlaceholder = new ArrayList<>();


    private ExpressionDeParserWrapper expressionVisitor = new ExpressionDeParserWrapper(validPlaceholder, invalidPlaceholder,
            new SelectDeParser(new ExpressionDeParserWrapper(validPlaceholder, invalidPlaceholder, buffer), buffer)
            , buffer);

    public List<Integer> getValidPalceholderIndex() {
        return this.expressionVisitor.getValidPlaceholder();
    }

    public UpdateDeParserWrapper() {
        super();
        expressionVisitor.setBuffer(buffer);
    }

    public UpdateDeParserWrapper(ExpressionDeParser expressionVisitor, StringBuilder buffer) {
        super(expressionVisitor, buffer);
        expressionVisitor.setBuffer(buffer);
        super.setExpressionVisitor(expressionVisitor);
    }

    @Override
    public String deSelectParse(Update update, List<String> selectColumns) {

        if (CollectionUtil.isEmpty(selectColumns)) {
            throw new IllegalArgumentException("查询列不能为空");
        }

        if (update.getWithItemsList() != null && !update.getWithItemsList().isEmpty()) {
            buffer.append("WITH ");
            for (Iterator<WithItem> iter = update.getWithItemsList().iterator(); iter.hasNext(); ) {
                WithItem withItem = iter.next();
                buffer.append(withItem);
                if (iter.hasNext()) {
                    buffer.append(",");
                }
                buffer.append(" ");
            }
        }
        buffer.append("SELECT ");

        this.appendSelectColumns(buffer, selectColumns);

        if (update.getModifierPriority() != null) {
            buffer.append(update.getModifierPriority()).append(" ");
        }
        if (update.isModifierIgnore()) {
            buffer.append("IGNORE ");
        }

        buffer.append(" FROM ");

        buffer.append(update.getTable());
        if (update.getStartJoins() != null) {
            for (Join join : update.getStartJoins()) {
                if (join.isSimple()) {
                    buffer.append(", ").append(join);
                } else {
                    buffer.append(" ").append(join);
                }
            }
        }

        // buffer.append(" SET ");

        // int j=0;
        expressionVisitor.setIgnore(true);
        for (UpdateSet updateSet : update.getUpdateSets()) {
            //            if (j > 0) {
            //                buffer.append(", ");
            //            }
            //
            //            if (updateSet.isUsingBracketsForColumns()) {
            //                buffer.append("(");
            //            }
            //            for (int i = 0; i < updateSet.getColumns().size(); i++) {
            //                if (i > 0) {
            //                    buffer.append(", ");
            //                }
            //                updateSet.getColumns().get(i).accept(expressionVisitor);
            //            }
            //            if (updateSet.isUsingBracketsForColumns()) {
            //                buffer.append(")");
            //            }
            //
            //            buffer.append(" = ");
            //
            //            if (updateSet.isUsingBracketsForValues()) {
            //                buffer.append("(");
            //            }
            for (int i = 0; i < updateSet.getExpressions().size(); i++) {
                //                if (i > 0) {
                //                    buffer.append(", ");
                //                }
                Expression expression = updateSet.getExpressions().get(i);
                // 只关注?占位符
                if (expression instanceof JdbcParameter) {
                    updateSet.getExpressions().get(i).accept(expressionVisitor);
                }
            }
            //            if (updateSet.isUsingBracketsForValues()) {
            //                buffer.append(")");
            //            }
            //
            //            j++;
        }
        expressionVisitor.setIgnore(false);
        if (update.getFromItem() != null) {
            buffer.append(" FROM ").append(update.getFromItem());
            if (update.getJoins() != null) {
                for (Join join : update.getJoins()) {
                    if (join.isSimple()) {
                        buffer.append(", ");
                        new JoinDeParser().deParse(join,expressionVisitor,buffer);
                    } else {
                        buffer.append(" ");
                        new JoinDeParser().deParse(join,expressionVisitor,buffer);
                    }
                }
            }
        }

        if (update.getWhere() != null) {
            buffer.append(" WHERE ");
            update.getWhere().accept(expressionVisitor);
        }
        if (update.getOrderByElements() != null) {
            new OrderByDeParser(expressionVisitor, buffer).deParse(update.getOrderByElements());
        }
        if (update.getLimit() != null) {
            new LimitDeparser(buffer).deParse(update.getLimit());
        }

        if (update.isReturningAllColumns()) {
            buffer.append(" RETURNING *");
        } else if (update.getReturningExpressionList() != null) {
            buffer.append(" RETURNING ");
            for (Iterator<SelectExpressionItem> iter = update.getReturningExpressionList().iterator(); iter
                    .hasNext(); ) {
                buffer.append(iter.next().toString());
                if (iter.hasNext()) {
                    buffer.append(", ");
                }
            }
        }
        return buffer.toString();
    }

    public ExpressionVisitor getExpressionVisitor() {
        return expressionVisitor;
    }

    public void setExpressionVisitor(ExpressionDeParserWrapper visitor) {
        expressionVisitor = visitor;
    }

    @Override
    public void visit(OrderByElement orderBy) {
        orderBy.getExpression().accept(expressionVisitor);
        if (!orderBy.isAsc()) {
            buffer.append(" DESC");
        } else if (orderBy.isAscDescPresent()) {
            buffer.append(" ASC");
        }
        if (orderBy.getNullOrdering() != null) {
            buffer.append(' ');
            buffer.append(orderBy.getNullOrdering() == OrderByElement.NullOrdering.NULLS_FIRST ? "NULLS FIRST"
                    : "NULLS LAST");
        }
    }
}
