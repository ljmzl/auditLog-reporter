package com.auditlog.sql.wrapper;

import lombok.Getter;
import lombok.Setter;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;
import net.sf.jsqlparser.util.deparser.SelectDeParser;

import java.util.ArrayList;
import java.util.List;

public class ExpressionDeParserWrapper extends ExpressionDeParser {

    private boolean ignore = false;

    @Getter
    @Setter
    private List<Integer> validPlaceholder = new ArrayList<>();
    @Getter
    @Setter
    private List<Integer> invalidPlaceholder = new ArrayList<>();

    public ExpressionDeParserWrapper() {

    }

    public ExpressionDeParserWrapper(List<Integer> validPlaceholder, List<Integer> invalidPlaceholder, StringBuilder buffer) {
        super(null, buffer);
        this.validPlaceholder = validPlaceholder;
        this.invalidPlaceholder = invalidPlaceholder;
    }


    public ExpressionDeParserWrapper(List<Integer> validPlaceholder, List<Integer> invalidPlaceholder, SelectDeParser selectDeParser, StringBuilder buffer) {
        super(selectDeParser, buffer);
        this.validPlaceholder = validPlaceholder;
        this.invalidPlaceholder = invalidPlaceholder;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    @Override
    public void visit(JdbcParameter jdbcParameter) {
        Integer index = jdbcParameter.getIndex();
        if (!ignore) {
            super.visit(jdbcParameter);
            validPlaceholder.add(index);
        } else {
            invalidPlaceholder.add(index);
        }
    }
}
