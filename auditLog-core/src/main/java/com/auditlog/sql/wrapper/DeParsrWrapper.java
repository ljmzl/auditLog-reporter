package com.auditlog.sql.wrapper;

import net.sf.jsqlparser.statement.Statement;

import java.util.List;

public interface DeParsrWrapper<T extends Statement> {

    String deSelectParse(T statement, List<String> selectColumns);

    default void appendSelectColumns(StringBuilder buffer, List<String> selectColumns) {
        int index = 1;
        for (String selectColumn : selectColumns) {
            buffer.append(" ").append(selectColumn);
            if (index != selectColumns.size()) {
                buffer.append(",");
            }
            index++;
        }
    }
}
