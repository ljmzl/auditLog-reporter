package com.auditlog.sql.parser;

import com.auditlog.datasource.struct.StatementMetaData;
import net.sf.jsqlparser.statement.Statement;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 12:29
 * @version 1.0
 */
public interface StatementMetaParser<S extends Statement> {

    /**
     获取表的别名
     *
     * @return: java.lang.String
     */
    String getAlias();

    /**
     获取表名
     *
     * @return: java.lang.String
     */
    String getTableName();

    /**
     获取带别名的表名
     *
     * @return: java.lang.String
     */
    String getTableNameWithAlias();


    StatementMetaData getMetaData(S statement);
}
