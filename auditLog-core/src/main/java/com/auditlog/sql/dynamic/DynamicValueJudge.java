package com.auditlog.sql.dynamic;

import com.auditlog.datasource.ConnectionProxy;
import net.sf.jsqlparser.expression.Expression;

public interface DynamicValueJudge {
    boolean isDynamicValue(Expression expression, ConnectionProxy connectionProxy);
}
