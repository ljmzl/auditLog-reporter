package com.auditlog.sql.dynamic;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Data
@Builder
public class SequenceMeta {
    private String name;
    private String owner;
    private Long increment;
    private Long cacheSize;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SequenceMeta that = (SequenceMeta) o;
        return Objects.equals(name, that.name) && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }
}
