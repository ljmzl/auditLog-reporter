package com.auditlog.sql.dynamic;

import com.auditlog.datasource.ConnectionProxy;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;

/**
 * Sequence序列
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/13 21:33
 */
public class SequenceDynamicValue implements DynamicValueJudge {

    @Override
    public boolean isDynamicValue(Expression expression, ConnectionProxy connection) {
        if (expression instanceof Column) {
            Column column = (Column) expression;
            if (column.getTable() != null) {
                String name = column.getTable().getName();
                String columnName = column.getColumnName();
                if (OracleSequenceCache.isSequenceKey(columnName)) {
                    SequenceMeta sequenceMeta = OracleSequenceCache.getSequenceMeta(name, connection);
                    if (sequenceMeta != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
