package com.auditlog.sql.dynamic;

import com.auditlog.datasource.ConnectionProxy;
import net.sf.jsqlparser.expression.Expression;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/4 12:29
 */
public class DynamicValueJudgeRegistry {

    private DynamicValueJudgeRegistry() {
        this.registerJudge(new SequenceDynamicValue());
    }

    private static class SingletonHolder {
        private static final DynamicValueJudgeRegistry INSTANCE = new DynamicValueJudgeRegistry();
    }

    public static DynamicValueJudgeRegistry getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private List<DynamicValueJudge> dynamicValueJudges = new ArrayList<>();

    public void registerJudge(DynamicValueJudge judge) {
        dynamicValueJudges.add(judge);
    }

    public boolean isDynamicValue(Expression expression, ConnectionProxy connection) {
        return dynamicValueJudges.stream().anyMatch(judge -> judge.isDynamicValue(expression, connection));
    }
}
