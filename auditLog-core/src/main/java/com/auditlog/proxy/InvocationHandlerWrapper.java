package com.auditlog.proxy;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * 代理执行器包装器
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/2 15:05
 */
@Data
@AllArgsConstructor
public class InvocationHandlerWrapper implements InvocationHandler {

    private Object target;

    private InvocationHanlderInterceptor handlerInterceptor;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        boolean execute = handlerInterceptor.preHandle(this, method, args);
        Object result = null;
        if (execute) {
            try {
                result = method.invoke(this.target, args);
            } catch (Exception e) {
                handlerInterceptor.onException(this, method, args, e);
            } finally {
                handlerInterceptor.postHandle(this, method, args);
            }
        }
        return result;
    }
}
