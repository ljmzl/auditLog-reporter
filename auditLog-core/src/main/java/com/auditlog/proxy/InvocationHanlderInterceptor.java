package com.auditlog.proxy;

import java.lang.reflect.Method;


/**
 * 可通过动态修改目标对象来控制执行过程
 * @see InvocationHandlerWrapper
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/2 15:05
 */
public interface InvocationHanlderInterceptor {

    /**
     * 前处理
     *
     * @param handlerWrapper
     * @param method
     * @param args
     * @return: boolean
     */
    default boolean preHandle(InvocationHandlerWrapper handlerWrapper, Method method, Object[] args) {
        return true;
    }

    /**
     * 后处理
     *
     * @param handlerWrapper
     * @param method
     * @param args
     * @return: void
     */
    default void postHandle(InvocationHandlerWrapper handlerWrapper, Method method, Object[] args) {

    }

    /**
     * 异常处理，对于不需要特殊处理的异常，可以直接抛出
     *
     * @param handlerWrapper
     * @param method
     * @param args
     * @param throwable
     * @return: void
     */
    default void onException(InvocationHandlerWrapper handlerWrapper, Method method, Object[] args, Throwable throwable) throws Throwable {
        throw throwable;
    }
}
