package com.auditlog.proxy;

import com.auditlog.util.ReflectionUtil;

import java.lang.reflect.Proxy;
import java.util.Set;

/**
 * 生成jdk代理对象
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/2 15:04
 */
public class ProxyFactory {
    public static Object getProxy(Object target, InvocationHanlderInterceptor hanlderInterceptor) {
        Class<?> targetClass = target.getClass();
        Set<Class<?>> interfaces = ReflectionUtil.getInterfaces(targetClass);
        Class[] classes = interfaces.toArray(new Class[0]);
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                classes,
                new InvocationHandlerWrapper(target, hanlderInterceptor));
    }

}
