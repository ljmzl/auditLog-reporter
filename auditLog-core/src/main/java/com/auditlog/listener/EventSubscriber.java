package com.auditlog.listener;

import com.google.common.eventbus.Subscribe;

public interface EventSubscriber<T> {

    void onEvent(T t);
}
