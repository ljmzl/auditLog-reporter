package com.auditlog.listener;

import com.auditlog.datasource.context.ExecutionContext;
import lombok.Builder;
import lombok.Data;


/**
 * 日志变更事件
 * @author Zhiyang.Zhang
 * @date 2022/11/28 18:23
 * @version 1.0
 */
@Data
@Builder
public class ContextChangeEvent {
    private ExecutionContext context;
}
