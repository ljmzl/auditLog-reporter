package com.auditlog.listener;

import cn.hutool.json.JSONUtil;
import com.auditlog.handler.OperationHandlerRegistry;
import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TableChangeEventSubscriber implements EventSubscriber<TableChangeEvent> {

    @Override
    @Subscribe
    public void onEvent(TableChangeEvent event) {
        OperationHandlerRegistry.getInstance().getStorageHandler().store(event);
    }

}
