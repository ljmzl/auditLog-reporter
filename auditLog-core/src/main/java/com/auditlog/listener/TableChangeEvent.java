package com.auditlog.listener;

import com.auditlog.sql.statistics.SqlStatistician;
import com.auditlog.datasource.struct.TableChangeLog;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;


/**
 * 表变更事件
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/2 22:35
 */
@Data
@Builder
public class TableChangeEvent {
    private List<TableChangeLog> tableChangeLogs;
    private String userId;
    private String userName;
    private String operationId;
    private SqlStatistician statistician;
    private Throwable cause;
    private Map<String,Object> metaData;
    private String operationName;
    private String dbType;
    private String resourceId;

    public boolean isError() {
        return this.cause != null;
    }
}
