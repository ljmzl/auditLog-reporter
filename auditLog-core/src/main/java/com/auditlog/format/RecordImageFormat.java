package com.auditlog.format;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.datasource.struct.FieldChangeLog;
import com.auditlog.datasource.struct.RecordImage;
import com.auditlog.datasource.struct.RowChangeLog;

import java.util.List;
import java.util.Map;

/**
 * 处理多条sql操作同一条数据的输出结果
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/11/20 23:08
 */
public interface RecordImageFormat {

    SqlType getType();

    /**
     * 用来删除TableRecord中多余的行数据
     *
     * @param recordImage
     * @return: void
     */
    void format(RecordImage recordImage);

    /**
     * 获取操作日志
     *
     * @param recordImage
     * @return: java.util.Map<com.auditlog.datasource.db.SqlType, java.util.List < java.util.List < com.auditlog.datasource.struct.FieldChangeLog>>>
     */
    Map<SqlType, List<RowChangeLog>> getChangeLog(RecordImage recordImage) throws Exception;
}
