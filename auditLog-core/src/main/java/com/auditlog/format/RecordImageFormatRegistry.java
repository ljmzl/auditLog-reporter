package com.auditlog.format;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.format.impl.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RecordImageFormatRegistry {

    private Map<SqlType, RecordImageFormat> recordImageFormatMap = new HashMap<>();

    private RecordImageFormatRegistry() {
        register(new DeleteRecordImageFormat());
        register(new UpdateRecordImageFormat());
        register(new InsertDuplicateRecordImageFormat());
        register(new InsertRecordImageFormat());
    }

    private static class SingletonHolder {
        private static RecordImageFormatRegistry INSTANCE = new RecordImageFormatRegistry();
    }

    public static RecordImageFormatRegistry getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void register(RecordImageFormat recordImageFormat) {
        recordImageFormatMap.put(recordImageFormat.getType(), recordImageFormat);
    }

    public void register(SqlType sqlType, RecordImageFormat recordImageFormat) {
        recordImageFormatMap.put(sqlType, recordImageFormat);
    }

    public RecordImageFormat getRecordImageFormat(SqlType sqlType) {
        return Optional.ofNullable(recordImageFormatMap.get(sqlType)).orElse(new DefaultRecordImageFormat());
    }

}
