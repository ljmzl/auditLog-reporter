package com.auditlog.format.impl;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.datasource.struct.FieldChangeLog;
import com.auditlog.datasource.struct.RecordImage;
import com.auditlog.datasource.struct.RowChangeLog;
import com.auditlog.format.AbstractRecordImageFormat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/3 12:03
 * @version 1.0
 */
public class DefaultRecordImageFormat extends AbstractRecordImageFormat {

    @Override
    public void doFormat(RecordImage recordImage) {
        // nothing
    }

    @Override
    public Map<SqlType, List<RowChangeLog>> changeLog(RecordImage recordImage) {
        return new HashMap<>(1);
    }

    @Override
    public SqlType getType() {
        return SqlType.ALL;
    }
}
