package com.auditlog.format.impl;

import com.auditlog.datasource.db.SqlType;
import com.auditlog.datasource.struct.RecordImage;
import com.auditlog.datasource.struct.RowChangeLog;
import com.auditlog.format.AbstractRecordImageFormat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/3 12:09
 * @version 1.0
 */
public class InsertDuplicateRecordImageFormat extends AbstractRecordImageFormat {

    @Override
    public void doFormat(RecordImage recordImage) {
        // nothing
    }

    @Override
    public Map<SqlType, List<RowChangeLog>> changeLog(RecordImage recordImage) throws Exception {
        Map<SqlType, List<RowChangeLog>> result = new HashMap<>(2, 1);
        Map<SqlType, List<RowChangeLog>> insertChangeLog = this.buildInsertChangeLog(recordImage);
        Map<SqlType, List<RowChangeLog>> updateChangeLog = this.buildUpdateChangeLog(recordImage);
        result.putAll(updateChangeLog);
        result.putAll(insertChangeLog);
        return result;
    }

    @Override
    public SqlType getType() {
        return SqlType.INSERT_DUPLICATE;
    }
}
