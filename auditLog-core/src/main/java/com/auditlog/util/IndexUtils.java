package com.auditlog.util;

import java.util.*;
import java.util.stream.Collectors;

public class IndexUtils {

    /**
     * 查询target中各字段在searched中的位置（下标从1开始）
     *
     * @param target   要查询位置的集合
     * @param searched 被查找的集合
     * @return: java.util.List<java.lang.Integer>
     */
    public static List<Integer> getIndexList(List<String> target, List<String> searched) {
        Map<String, List<String>> targetMap = new HashMap<>();
        targetMap.put("DEFAULT", target);
        Map<String, List<Integer>> indexList = getIndexList(targetMap, searched);
        if (indexList.size() == 0) {
            return new ArrayList<>();
        }
        return indexList.get("DEFAULT");
    }

    public static Map<String, List<Integer>> getIndexList(Map<String, List<String>> target, List<String> searched) {
        Map<String, List<Integer>> indexMap = new LinkedHashMap<>();
        for (Map.Entry<String, List<String>> entry : target.entrySet()) {
            List<String> valueList = entry.getValue();
            List<Integer> indexes = new ArrayList<>();
            for (String colName : valueList) {
                int i = 1; // 这里从1开始，是因为查询出来的值放入valueMap是以1开始的
                for (String column : searched) {
                    if (colName.equals(column)) {
                        indexes.add(i);
                    }
                    i++;
                }
            }
            if (indexes.size() == valueList.size()) {
                indexMap.put(entry.getKey(), indexes);
            }
        }
        return indexMap;
    }

    public static Integer getMaxNumber(List<Integer> numberList) {
        return numberList.stream().collect(Collectors.maxBy(Comparator.comparingInt(v -> v))).orElseThrow(() -> new IllegalStateException("非法参数"));
    }

    public static Integer getMinNumber(List<Integer> numberList) {
        return numberList.stream().collect(Collectors.minBy(Comparator.comparingInt(v -> v))).orElseThrow(() -> new IllegalStateException("非法参数"));
    }
}
