package com.auditlog.util;

public class EnvUtils {

    private static final String className = "oracle.jdbc.driver.OracleDriver";

    private static Boolean isOracleDb = null;

    public static synchronized boolean isOracleDb() {
        if (isOracleDb == null) {
            ClassLoader cl = null;
            try {
                cl = Thread.currentThread().getContextClassLoader();
            } catch (Throwable ex) {
            }
            if (cl == null) {
                cl = EnvUtils.class.getClassLoader();
                if (cl == null) {
                    try {
                        cl = ClassLoader.getSystemClassLoader();
                    } catch (Throwable ex) {
                    }
                }
            }
            try {
                if (cl != null) {
                    cl.loadClass(className);
                } else {
                    Class.forName(className);
                }
                isOracleDb = true;
            } catch (Throwable throwable) {
                isOracleDb = false;
            }
        }
        return isOracleDb;
    }
}
