package com.auditlog.util;

import cn.hutool.core.collection.CollectionUtil;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;

import java.util.Map;

public class ExpressionUtils {

    /**
     * 获取表达式的值
     *
     * @param expression 表达式
     * @param parameters 表达式中参数对应的值
     * @return: java.lang.Object
     */
    public static Object getValue(String expression, Map<String, Object> parameters) throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        if (CollectionUtil.isNotEmpty(parameters)) {
            parameters.forEach((key, value) -> context.put(key, value));
        }
        return runner.execute(expression, context, null, true, false);
    }

    public static Object getValue(String expression) throws Exception {
        return getValue(expression, null);
    }
}
