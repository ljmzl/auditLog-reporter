package com.auditlog.handler.meta;

import java.util.Map;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 11:37
 * @version 1.0
 */
public interface UserOperationHandler {

    /**
     获取当前操作人id
     *
     * @return: java.lang.String
     */
    String userId();

    /**
     获取当前操作人名称
     *
     * @return: java.lang.String
     */
    String userName();

    Map<String,Object> metaData();
}
