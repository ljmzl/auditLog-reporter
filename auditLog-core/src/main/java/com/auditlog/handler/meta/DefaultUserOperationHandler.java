package com.auditlog.handler.meta;

import java.util.Collections;
import java.util.Map;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 20:45
 * @version 1.0
 */
public class DefaultUserOperationHandler implements UserOperationHandler {
    @Override
    public String userId() {
        return "-1";
    }

    @Override
    public String userName() {
        return "系统默认";
    }

    @Override
    public Map<String, Object> metaData() {
        return Collections.emptyMap();
    }
}
