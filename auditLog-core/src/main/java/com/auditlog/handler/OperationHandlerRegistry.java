package com.auditlog.handler;

import cn.hutool.core.lang.Assert;
import com.auditlog.handler.meta.DefaultUserOperationHandler;
import com.auditlog.handler.meta.UserOperationHandler;
import com.auditlog.handler.storage.LoggerStorageHandler;
import com.auditlog.handler.storage.StorageHandler;

/**
 * trace元数据
 *
 * @author Zhiyang.Zhang
 * @version 1.0
 * @date 2022/12/2 22:07
 */
public class OperationHandlerRegistry {

    private UserOperationHandler userOperationHandler = new DefaultUserOperationHandler();

    private StorageHandler storageHandler = new LoggerStorageHandler();

    private OperationHandlerRegistry() {
    }

    private static class SingletonHolder {
        private static final OperationHandlerRegistry INSTANCE = new OperationHandlerRegistry();
    }

    public static OperationHandlerRegistry getInstance() {
        return OperationHandlerRegistry.SingletonHolder.INSTANCE;
    }

    public void registerUserOperationHandler(UserOperationHandler userOperationHandler) {
        Assert.notNull(userOperationHandler, "对象不能为空");
        this.userOperationHandler = userOperationHandler;
    }

    public void registerStorageHandler(StorageHandler storageHandler) {
        Assert.notNull(storageHandler, "对象不能为空");
        this.storageHandler = storageHandler;
    }

    public UserOperationHandler getOperationUserHandler() {
        return this.userOperationHandler;
    }

    public StorageHandler getStorageHandler() {
        return this.storageHandler;
    }
}
