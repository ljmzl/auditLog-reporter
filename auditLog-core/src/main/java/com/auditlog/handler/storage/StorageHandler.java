package com.auditlog.handler.storage;

import com.auditlog.listener.TableChangeEvent;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/3 17:41
 * @version 1.0
 */
public interface StorageHandler {
    /**
     存储
     * @param event 日志变更事件
     * @return: boolean
     */
    boolean store(TableChangeEvent event);
}
