package com.auditlog.handler.storage;

import cn.hutool.json.JSONUtil;
import com.auditlog.listener.TableChangeEvent;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Zhiyang.Zhang
 * @date 2022/12/4 22:26
 * @version 1.0
 */
@Slf4j
public class LoggerStorageHandler implements StorageHandler {
    @Override
    public boolean store(TableChangeEvent event) {
        log.warn("====>{}", JSONUtil.toJsonStr(event));
        return false;
    }
}
