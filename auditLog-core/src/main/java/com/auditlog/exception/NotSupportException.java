package com.auditlog.exception;

public class NotSupportException extends RuntimeException {

    public NotSupportException(String message) {
        super(message);
    }

    public NotSupportException(Throwable cause) {
        super(cause);
    }
}
