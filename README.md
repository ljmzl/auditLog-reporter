# auditLog-reporter

如果这个项目对你有所帮助，记得  :heart: Star 关注 :heart: 哦，这是对我最大的支持与鼓励。

#### 背景

在系统建设中，为了保护某些接口，往往要求对接口中一些重要数据的变更进行溯源，怎么办？

<u>**auditLog-reporter**</u>能帮助你实现对数据的跟踪，让操作无处遁形！！！

#### 介绍

1. 目前支持Mysql、Oracle、Postgres，后续会根据反馈情况进行更多数据库适配，请持续关注；
2. 本项目旨在解析sql更新语句执行前后，表数据的变更日志，并提取出变更的字段信息，供用户作后续处理；
3. 本项目不适合高并发场景，高并发系统请移步canal、flink等处理方案；
4. 对于springboot项目零侵入性，只需在方法上加上`@AuditLog`注解即可实现对审计日志的变更记录；
5. 进行日志处理时，附带了sql统计，供优化使用；
6. 支持对复杂sql的解析处理；
7. 对数据库连接的处理参照了[seata](https://github.com/seata/seata)，并作了小修改，如有侵权请及时联系删除，谢谢！

#### 使用说明

1.  添加依赖

    ```xml
    <dependency>
       <groupId>com.zzy</groupId>
       <artifactId>auditLog-springboot-starter</artifactId>
       <version>1.0</version>
    </dependency>
    ```

2.  向springboot容器中注入一个`com.auditlog.handler.storage.StorageHandler`实现类，即可对变更日志实现自定义操作；

3.  向springboot容器中注入`com.auditlog.handler.meta.UserOperationHandler`实现类，即可添加元数据信息（比如：操作人等）；

4.  在需要记录操作日志的方法上加上@AuditLog注解；

5.  项目中内置了一些Converter，如不满足需要，可向springboot容器注入多个相应的`com.auditlog.converter.Converter`实现类来满足个性化需求。

#### 参与贡献

欢迎给出优化建议以及指出bug，请清楚描述遇到的问题，通过提issue或者直接联系作者讨论，感激不尽。

#### 联系作者

 :email: 邮箱： **13169639@qq.com** 

QQ交流群：773938960
