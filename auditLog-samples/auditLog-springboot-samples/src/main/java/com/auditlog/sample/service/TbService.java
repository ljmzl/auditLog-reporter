package com.auditlog.sample.service;

import com.auditlog.boot.annotations.AuditLog;
import com.auditlog.sample.entity.Tb;
import com.auditlog.sample.mapper.TableMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

@Service
public class TbService extends ServiceImpl<TableMapper, Tb> {

    @AuditLog(operationName = "测试中")
    @Override
    public boolean saveBatch(Collection<Tb> entityList, int batchSize) {
        return super.saveBatch(entityList, batchSize);
    }

    @AuditLog
    public void testComplexSql(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("insert into multi_op (id,name) select pk1,pk2 from two_pk_1 where pk2 = ? ;" +
                "update TWO_PK_1 tp set tp.name = ? where tp.pk2 = ?;" +
                "update MULTI_OP t set t.name = 'zhangsan' where t.id = 4;" +
                "update multi_op tt set tt.name = 'lisi' where tt.name = ?;" +
                "delete sp from ORDER_TBL sp " +
                "     inner join multi_op  ds " +
                "     on sp.id  = ds.id " +
                "     and sp.commodity_code  = ?;" +
                "insert into two_pk_1 (pk1,pk2,`desc`) with temp as ( select id,'zhangyang',name from multi_op where id in (?)) select * from temp;" +
                "update order_tbl a join multi_op b on a.id =b.id set a.commodity_code = ? where b.name =? ");
        preparedStatement.setObject(1, "2");
        preparedStatement.setObject(2, "zzzz");
        preparedStatement.setObject(3, "2");
        preparedStatement.setObject(4, "2");
        preparedStatement.setObject(5, "3");
        preparedStatement.setObject(6, 111);
        preparedStatement.setObject(7, "commodity");
        preparedStatement.setObject(8, "222");
        preparedStatement.addBatch();
        preparedStatement.setObject(1, "3");
        preparedStatement.setObject(2, "zzzz");
        preparedStatement.setObject(3, "2");
        preparedStatement.setObject(4, "22");
        preparedStatement.setObject(5, "1");
        preparedStatement.setObject(6, 333);
        preparedStatement.setObject(7, "code");
        preparedStatement.setObject(8, "444");
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
    }
}
