package com.auditlog.sample.controller;

import com.auditlog.sample.entity.Tb;
import com.auditlog.sample.service.TbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("op")
@Transactional(rollbackFor = {Exception.class})
public class SampleController extends ConnectionHolder {


    @Autowired
    TbService tbService;

    @RequestMapping("complex")
    public Object insert4() throws SQLException {
        tbService.testComplexSql(getConnection());
        return "ok";
    }

    @RequestMapping("insert")
//    @AuditLog(operationName = "测试")
    //联合主键
    public Object insert() {
        List<Tb> tbs = new ArrayList<>();
        for (int i = 1; i <= 10000; i++) {
            Tb tb = new Tb();
            tb.setPk_2(i);
            tb.setUk1_1(String.valueOf(i));
            tb.setUk1_2(String.valueOf(i));
            tb.setUk2(i);
            tb.setColumn1(String.valueOf(i));
            tb.setColumn2(LocalDateTime.now());
            tbs.add(tb);
        }
        tbService.saveBatch(tbs, 100);
        return "ok";
    }
}
