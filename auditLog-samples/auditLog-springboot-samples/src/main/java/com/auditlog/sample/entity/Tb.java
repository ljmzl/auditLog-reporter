package com.auditlog.sample.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName("tb1")
@Data
public class Tb {
    private Integer pk_1;
    private Integer pk_2;
    private String uk1_1;
    private String uk1_2;
    private Integer uk2;
    private String column1;
    private LocalDateTime column2;
}
