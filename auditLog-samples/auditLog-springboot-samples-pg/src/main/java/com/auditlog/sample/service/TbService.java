package com.auditlog.sample.service;

import com.auditlog.sample.entity.Test;
import com.auditlog.sample.mapper.TableMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class TbService extends ServiceImpl<TableMapper, Test> {

}
