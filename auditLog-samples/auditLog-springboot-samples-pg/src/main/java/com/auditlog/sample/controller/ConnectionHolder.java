package com.auditlog.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;

public class ConnectionHolder {

    @Autowired
    DataSource dataSource;

    protected Connection getConnection() {
        return DataSourceUtils.getConnection(dataSource);
    }
}
