package com.auditlog.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PgApp {
    public static void main(String[] args) {
        SpringApplication.run(PgApp.class, args);
    }
}
