package com.auditlog.sample.mapper;

import com.auditlog.sample.entity.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TableMapper extends BaseMapper<Test> {
}
