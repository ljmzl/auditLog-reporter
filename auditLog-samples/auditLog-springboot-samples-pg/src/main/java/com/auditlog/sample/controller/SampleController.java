package com.auditlog.sample.controller;

import com.auditlog.boot.annotations.AuditLog;
import com.auditlog.sample.entity.Test;
import com.auditlog.sample.service.TbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@RestController
@RequestMapping("op")
//@Transactional(rollbackFor = {Exception.class})
public class SampleController extends ConnectionHolder {


    @Autowired
    TbService tbService;

    @RequestMapping("insert")
    @AuditLog
    public Object insert() throws SQLException {
        Test test = new Test();
        test.setC1("1");
        test.setC3("1");
        test.setId("1");
        tbService.save(test);
        Test test2= new Test();
        test2.setC1("2");
        test2.setC3("2");
        test2.setId("2");
        tbService.save(test2);
        return "ok";
    }

    @RequestMapping("update")
    @AuditLog
    @Transactional(rollbackFor = Exception.class)
    public Object update() throws SQLException {
        Test test = new Test();
        test.setC1("11");
        test.setC3("11");
        test.setId("1");
        tbService.updateById(test);
        Test test2 = new Test();
        test2.setC1("22");
        test2.setC2("22");
        test2.setId("2");
        tbService.updateById(test2);
        Test test3 = new Test();
        test3.setC1("3");
        test3.setC3("3");
        test3.setId("3");
        tbService.save(test3);
        return "ok";
    }

}
