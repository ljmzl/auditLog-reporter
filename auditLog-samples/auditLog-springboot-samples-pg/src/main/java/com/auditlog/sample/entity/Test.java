package com.auditlog.sample.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName("test.test")
@Data
public class Test {
    @TableId(type = IdType.AUTO)
    private String id;
    private String c1;
    private String c2;
    private String c3;
    private String createTime;
    private String updateTime;
}
